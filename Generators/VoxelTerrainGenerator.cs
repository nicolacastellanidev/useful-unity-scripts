﻿using System;
using System.Collections.Generic;
using System.Linq;
using Controllers.Voxels;
using Models;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Generators
{
    [ExecuteInEditMode]
    public class VoxelTerrainGenerator : MonoBehaviour
    {
        [SerializeField] private Vector2 terrainSize = new Vector2(32, 32);
        [SerializeField] private Vector3 voxelSize = new Vector3(1, 1, 1);
        [SerializeField] private TerrainVoxelController[] voxelPrefabs = null;
        [SerializeField] private LayerMask checkLayerMask = 0;
        [SerializeField] private Transform floorVoxelParent;
        [Range(1, 20)]
        [SerializeField] private int minDropProbability = 0;
        [Range(1, 20)]
        [SerializeField] private int maxDropProbability = 0;


        private List<List<Voxel>> vox_matrix = new List<List<Voxel>>();
        private float childCount = 0;
        
        public void Generate(float yDelta = 0f)
        {
            ClearTerrain();
            GenerateTerrain();
        }

        public void ClearTerrain()
        {
            childCount = 0;
            foreach (var voxel in vox_matrix.SelectMany(row => row))
            {
#if UNITY_EDITOR
                DestroyImmediate(voxel.gameObject);
#else
                Destroy(voxel.gameObject);
#endif
            }

            var others = GameObject.FindGameObjectsWithTag("Floor");

            foreach (var go in others)
            {
#if UNITY_EDITOR
                DestroyImmediate(go);
#else
                Destroy(go);
#endif
            }
            
            vox_matrix.Clear();
        }

        private void GenerateTerrain()
        {
            GenerateRandom();
            for (var rowIndex = -terrainSize.x / 2; rowIndex < terrainSize.x / 2; rowIndex++)
            {
                vox_matrix.Add(new List<Voxel>((int)terrainSize.x));
                GenerateRow(rowIndex);
            }
            print("Generated " + childCount + "voxels.");
        }

        // generates a few random voxels
        private void GenerateRandom()
        {
            var localScale = transform.localScale;
            while (childCount < terrainSize.y)
            {
                var randomRow = Random.Range(0, (int)terrainSize.x);
                var position = new Vector3(
                    voxelSize.x * Random.Range(-(int)(terrainSize.y / 2f), (int)(terrainSize.y / 2f)) * localScale.x, 
                    0, 
                    voxelSize.z * Random.Range(-(int)(terrainSize.x / 2f), (int)(terrainSize.x / 2f)) * localScale.z);
                if (Physics.OverlapBox(position, new Vector3(voxelSize.x/4, voxelSize.y * 2, voxelSize.z/4), Quaternion.identity, checkLayerMask).Length != 0) continue;
                while (vox_matrix.Count <= randomRow)
                {
                    vox_matrix.Add(new List<Voxel>((int) terrainSize.y));
                }
                vox_matrix[randomRow].Add(GenerateVoxelAt(position, (int)position.x, (int)position.z, true));
            }
        }

        private void GenerateRow(float rowIndex)
        {
            for (var colIndex = -terrainSize.y/2; colIndex < terrainSize.y/2; colIndex++)
            {
                var localScale = transform.localScale;
                var position = new Vector3(voxelSize.x * colIndex * localScale.x, 0, voxelSize.z * rowIndex * localScale.z);
                if (Physics.OverlapBox(position, new Vector3(voxelSize.x/4, voxelSize.y * 2, voxelSize.z/4), Quaternion.identity, checkLayerMask).Length != 0) continue;
                vox_matrix[vox_matrix.Count - 1].Add(GenerateVoxelAt(position, (int)rowIndex, (int)colIndex));
            }
        }

        private Voxel GenerateVoxelAt(Vector3 position, int rowIndex, int colIndex, bool pureRandom = false)
        {
            var go = Instantiate(
                pureRandom ? voxelPrefabs[Random.Range(0, voxelPrefabs.Length)].gameObject : GetPrefabForPosition(rowIndex, colIndex),
                position,
                Quaternion.Euler( 0, (int)(Random.Range( 0, 4 ) * 90), 0 ),
                floorVoxelParent
            );
                
            var voxel = go.GetComponent<VoxelController>().data;
            voxel.Set(position, voxelSize, go,  rowIndex > -1);
            go.GetComponent<VoxelController>().data = voxel;
                
            go.transform.localScale = voxelSize;

            childCount++;
            
            go.GetComponent<VoxelController>().Init();
            
            return voxel;
        }

        private GameObject GetPrefabForPosition(int rowIndex, int colIndex, int recursionIndex = 0)
        {
            var randomVoxel = voxelPrefabs[Random.Range(0, voxelPrefabs.Length)].gameObject;
            var voxel = randomVoxel.GetComponent<VoxelController>().data;
            var dropProbability = minDropProbability;
            // calc probability
            // get element near this position
            for (var i = rowIndex - 1; i < rowIndex + 1; i++)
            {
                for (var j = colIndex - 1; j < colIndex + 1; j++)
                {
                    // check for edge cases
                    if (!(i >= 0) || !(i < vox_matrix.Count)) continue;
                    if (j < 0 || j >= vox_matrix[i].Count) continue;
                    
                    var targetVoxel = vox_matrix[i][j];

                    if (targetVoxel == null)
                    {
                        dropProbability++;
                        continue;
                    };
                    
                    if(targetVoxel.type == voxel.type)
                    {
                        dropProbability++;
                    }

                    if (targetVoxel.gameObject.name == randomVoxel.gameObject.name)
                    {
                        dropProbability += 2;
                    }
                }
            }

            dropProbability = Mathf.Clamp(dropProbability, minDropProbability, maxDropProbability);

            return recursionIndex > 5 ? GetRandomByMinScore() :
                dropProbability >= voxel.minScoreToGenerate ? randomVoxel : GetPrefabForPosition(rowIndex, colIndex, ++recursionIndex);
        }

        private GameObject GetRandomByMinScore()
        {
            var targets = (from go in voxelPrefabs where go.data.minScoreToGenerate <= minDropProbability select go.gameObject).ToList();
            return targets[Random.Range(0, targets.Count)];
        }
    }
}