﻿using System;
using UnityEngine;

namespace Models
{
    public enum VoxelType
    {
        GRASS,
        SAND,
        ROCK
    }
    
    [Serializable]
    public class Voxel
    {
        public int minScoreToGenerate = 2;
        public VoxelType type;
        public Vector3 position;
        public Vector3 size; // in meters
        public GameObject gameObject;
        public bool enemy = false;

        public void Set(Vector3 position, Vector3 size, GameObject gameObject, bool enemy)
        {
            this.position = position;
            this.size = size;
            this.gameObject = gameObject;
            this.enemy = enemy;
        }
    }
}